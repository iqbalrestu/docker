FROM golang:latest

# RUN go get github.com/gin-gonic/gin && \
# 	go get gitlab.com/iqbalrestu/docker

# # Add Maintainer Info
# LABEL maintainer="iqbal restu maulana"

# WORKDIR $GOPATH/src/gitlab.com/iqbalrestu/docker
# COPY . .
# RUN go get -d -v ./...

# # Install the package
# RUN go install -v ./...
# WORKDIR $GOPATH/src/gitlab.com/iqbalrestu/docker

# COPY . .
# RUN go build -v

# FROM buildpack-deps:jessie

# WORKDIR /usr/local/bin

# COPY --from=builder $GOPATH/src/gitlab.com/iqbalrestu/docker .
# CMD ["./docker"]

# Add Maintainer Info
LABEL maintainer="iqbal"

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/gitlab.com/iqbalrestu/docker

# Copy everything from the current directory to the PWD(Present Working Directory) inside the container
COPY . .

# Download all the dependencies
RUN go get -d -v ./...

# Install the package
RUN go install -v ./...

# This container exposes port 8080 to the outside world
EXPOSE 7700

# Run the executable
CMD ["./compile"]
EXPOSE 7700